# -*- coding: utf-8 -*-
import os
import aioboto3

from shitify.models.ParameterStoreConfiguration import ParameterStoreConfiguration


SHITIFY_ENVIRONMENT_VARIABLE_NAME = "ShitifyEnvironment"
STRING_TYPE_NAME = "String"
STRING_LIST_TYPE_NAME = "StringList"


class AwsParameterStore:

    application_name = ""
    parameters_path = ""
    type_handlers = {}

    def __init__(self, application_name):
        self.application_name = application_name
        self.parameters_path = "/%s/%s" % (self._get_context(), self.application_name)
        self.type_handlers = {
            STRING_TYPE_NAME: lambda x: self._handle_string(x),
            STRING_LIST_TYPE_NAME: lambda x: self._handle_string_list(x)
        }

    @staticmethod
    def _get_client():
        return aioboto3.client('ssm')

    @staticmethod
    def _get_context():
        return os.environ.get(SHITIFY_ENVIRONMENT_VARIABLE_NAME, "Production")

    def _path_converter(self, path):
        option_name = path[len(self.parameters_path):]
        return option_name.replace("/", "_").upper()

    @staticmethod
    def _handle_string(value):
        return value

    @staticmethod
    def _handle_string_list(value):
        return value.split(",")

    def _convert_to_dictionary(self, parameters):
        result = {}
        for param in parameters:
            key = self._path_converter(param.get("Name"))
            handler = self.type_handlers.get(param.get("Type"), self.type_handlers.get(STRING_TYPE_NAME))
            value = handler(param.get("Value"))
            result[key] = value
        return result

    async def get_configuration(self):
        async with self._get_client() as client:
            parameters = await client.get_parameters_by_path(Path=self.parameters_path, Recursive=True)
            if parameters is None:
                return ParameterStoreConfiguration({})
            return ParameterStoreConfiguration(self._convert_to_dictionary(parameters.get("Parameters", [])))
