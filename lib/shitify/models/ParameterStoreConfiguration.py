# -*- coding: utf-8 -*-


class ParameterStoreConfiguration:
    def __init__(self, values):
        self.__dict__.update(values)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__
