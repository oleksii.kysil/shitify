# -*- coding: utf-8 -*-
import asynctest
from unittest.mock import MagicMock

from shitify.repositories.AwsParameterStore import AwsParameterStore
from shitify.models.ParameterStoreConfiguration import ParameterStoreConfiguration


class AwsParameterStoreTest(asynctest.TestCase):

    client_mock = None

    def setUp(self):
        AwsParameterStore._get_context = MagicMock()
        AwsParameterStore._get_context.return_value = "Test"
        instance = AwsParameterStore("test")
        client_mock = asynctest.mock.MagicMock()
        self.client_mock = client_mock
        instance._get_client = MagicMock()
        instance._get_client.return_value = client_mock
        client_mock.__aenter__.return_value = client_mock
        coroutine_mock = asynctest.mock.CoroutineMock()
        client_mock.get_parameters_by_path = coroutine_mock
        coroutine_mock.return_value = {
            "Parameters": [
                {
                    "Type": "String",
                    "Value": "Test1",
                    "Name": "Test/test/param1/param1"
                },
                {
                    "Type": "StringList",
                    "Value": "Test2,Test3",
                    "Name": "Test/test/param2/param2"
                },
                 {
                    "Type": "Unknown",
                    "Value": "Test4",
                    "Name": "Test/test/param3/param3"
                },
            ]
        }
        self.instance = instance

    async def test_get_configuration_success(self):
        result = await self.instance.get_configuration()
        self.assertEqual(result, ParameterStoreConfiguration({
            "PARAM1_PARAM1": "Test1",
            "PARAM2_PARAM2": ["Test2", "Test3"],
            "PARAM3_PARAM3": "Test4",

        }))

    async def test_get_configuration_failure(self):
        async def _get_parameters_by_path_mock_no_value(**kwargs):
            return None
        self.client_mock.get_parameters_by_path = _get_parameters_by_path_mock_no_value
        result = await self.instance.get_configuration()
        self.assertEqual(result, ParameterStoreConfiguration({}))


if __name__ == '__main__':
    asynctest.main()
